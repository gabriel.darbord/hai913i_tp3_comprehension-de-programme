package processing.moduleidentifier.clustertree;

import java.util.ArrayList;
import java.util.HashMap;

import utils.Utils;

// noeud pouvant être un cluster ou une classe
public abstract class ClusterNode {
	private boolean isLeaf; // une classe est une feuille

	protected ClusterNode(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	// retourne le poids couplage de ce cluster
	public abstract double getWeight();

	// retourne le poids du couplage entre deux clusters
	public double getWeightWith(ClusterNode other, HashMap<String, HashMap<String, Double>> relationWeights) {
		ArrayList<Class> thisLeaves = gatherLeaves();
		ArrayList<Class> otherLeaves = other.gatherLeaves();
		double weight = 0;

		for (int i = 0; i < thisLeaves.size(); i++) {
			String class1 = thisLeaves.get(i).toString();

			for (int j = 0; j < otherLeaves.size(); j++) {
				String class2 = otherLeaves.get(j).toString();

				weight += relationWeights.get(class1).getOrDefault(class2, 0.);
				weight += relationWeights.get(class2).getOrDefault(class1, 0.);
			}
		}

		return weight;
	}

	// récupère toutes les classes du cluster
	public ArrayList<Class> gatherLeaves() {
		ArrayList<Class> leaves = new ArrayList<>();
		gatherLeaves(leaves);
		return leaves;
	}

	protected abstract void gatherLeaves(ArrayList<Class> leaves);

	// retourne une représentation du cluster dans le langage 'dot'
	public String toGraph() {
		StringBuilder sb = new StringBuilder(Utils.GRAPH_HEADER);
		toGraph(sb);
		return sb.append('}').toString();
	}

	protected abstract void toGraph(StringBuilder sb);

	@Override
	public abstract String toString();
}
