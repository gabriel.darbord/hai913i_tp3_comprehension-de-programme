package processing.moduleidentifier.clustertree;

import java.util.ArrayList;

import utils.Utils;

// noeud contenant deux noeuds fils
public class Cluster extends ClusterNode {
	private ClusterNode left;
	private ClusterNode right;
	private double weight;

	public Cluster(ClusterNode left, ClusterNode right, double weight) {
		super(false);
		this.left = left;
		this.right = right;
		this.weight = weight;
	}

	public ClusterNode getLeft() {
		return left;
	}

	public ClusterNode getRight() {
		return right;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	protected void gatherLeaves(ArrayList<Class> leaves) {
		left.gatherLeaves(leaves);
		right.gatherLeaves(leaves);
	}

	@Override
	protected void toGraph(StringBuilder sb) {
		String thisName = "\"" + toString() + "\"";
		sb.append(thisName + " [label=\"" + Utils.DECIMAL_FORMATTER.format(weight) + "\", style=text, margin=0]\n");
		sb.append(thisName + " -- \"" + left + "\"\n");
		sb.append(thisName + " -- \"" + right + "\"\n");
		left.toGraph(sb);
		right.toGraph(sb);
	}

	@Override
	public String toString() {
		return "Cluster[" + left + ", " + right + "]";
	}
}
