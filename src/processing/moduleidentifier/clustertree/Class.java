package processing.moduleidentifier.clustertree;

import java.util.ArrayList;

// feuille de l'arbre, correspond à une classe
public class Class extends ClusterNode {
	private String name;

	public Class(String name) {
		super(true);
		this.name = name;
	}

	@Override
	public double getWeight() {
		return 0; // une classe seule n'a pas de poids de couplage
	}

	@Override
	protected void gatherLeaves(ArrayList<Class> leaves) {
		leaves.add(this);
	}

	@Override
	protected void toGraph(StringBuilder sb) {
		sb.append("\"" + name + "\"\n");
	}

	@Override
	public String toString() {
		return name;
	}
}
