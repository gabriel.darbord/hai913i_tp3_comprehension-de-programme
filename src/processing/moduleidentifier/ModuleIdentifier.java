package processing.moduleidentifier;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;

import processing.moduleidentifier.clustertree.Class;
import processing.moduleidentifier.clustertree.Cluster;
import processing.moduleidentifier.clustertree.ClusterNode;

public class ModuleIdentifier {
	// poids des relations entre classes
	private HashMap<String, HashMap<String, Double>> relationWeights;

	public ModuleIdentifier(HashMap<String, HashMap<String, Double>> relationWeights) {
		this.relationWeights = relationWeights;
	}

	/*
	 * =============================================================================
	 * === Clustering
	 * =============================================================================
	 */

	public ClusterNode clustering() {
		ArrayList<ClusterNode> clusters = new ArrayList<>();

		// chaque classe est une feuille
		for (String className : relationWeights.keySet()) {
			clusters.add(new Class(className));
		}

		while (clusters.size() > 1) {
			Cluster newCluster = nearestCluster(clusters);
			clusters.remove(newCluster.getLeft());
			clusters.remove(newCluster.getRight());
			clusters.add(newCluster);
		}

		return clusters.get(0);
	}

	// retourne un nouveau cluster constitué des deux clusters les plus couplés
	// 'clusters' est garanti de taille > 1 donc on trouve forcément un résultat
	private Cluster nearestCluster(ArrayList<ClusterNode> clusters) {
		ClusterNode left = null, right = null;
		double maxWeight = -1;

		for (int i = 0; i < clusters.size(); i++) {
			ClusterNode tmp1 = clusters.get(i);

			for (int j = i + 1; j < clusters.size(); j++) {
				ClusterNode tmp2 = clusters.get(j);

				// calcule du poids
				double weight = tmp1.getWeightWith(tmp2, relationWeights);

				// nouveau max
				if (maxWeight < weight) {
					left = tmp1;
					right = tmp2;
					maxWeight = weight;
				}
			}
		}

		return new Cluster(left, right, maxWeight);
	}

	/*
	 * =============================================================================
	 * === Groupes de classes couplées
	 * =============================================================================
	 */

	public ArrayList<ClusterNode> partitioning(ClusterNode tree) {
		ArrayList<ClusterNode> partition = new ArrayList<>();
		ArrayDeque<ClusterNode> parkour = new ArrayDeque<>();

		parkour.addFirst(tree);

		while (!parkour.isEmpty()) {
			ClusterNode parentNode = parkour.removeFirst();

			if (parentNode.isLeaf()) {
				partition.add(parentNode);
				continue;
			}

			Cluster parent = (Cluster) parentNode;
			ClusterNode c1 = parent.getLeft();
			ClusterNode c2 = parent.getRight();

			if (parent.getWeight() > (c1.getWeight() + c2.getWeight() / 2)) {
				partition.add(parent);
			} else {
				parkour.addFirst(c1);
				parkour.addFirst(c2);
			}
		}

		return partition;
	}

	/*
	 * =============================================================================
	 * === Structures de données
	 * =============================================================================
	 */

}
