package processing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import graphers.GraphvizGrapher;
import processing.moduleidentifier.ModuleIdentifier;
import processing.moduleidentifier.clustertree.ClusterNode;
import utils.Utils;

public abstract class AbstractASTProcessor {
	private String projectId;

	// résultat des visiteurs
	protected HashMap<String, HashMap<String, Double>> weightedCoupling;
	protected int nbRelations;
	protected ClusterNode clustering;
	protected ArrayList<ClusterNode> partition;

	/*
	 * =============================================================================
	 * === Analyse
	 * =============================================================================
	 */

	// méthode à surcharger pour faire l'analyse du programme
	public void analyze(String projectPath) throws IOException {
		projectId = projectPath.replaceAll("[" + File.separator + ": ]", "_");
	}
	
	protected void couplingAnalysis() {
		ModuleIdentifier mi = new ModuleIdentifier(weightedCoupling);
		clustering = mi.clustering();
		partition = mi.partitioning(clustering);
	}

	/*
	 * =============================================================================
	 * === Métriques
	 * =============================================================================
	 */

	public HashMap<String, HashMap<String, Double>> getWeightedCoupling() {
		return weightedCoupling;
	}

	public int getNbRelations() {
		return nbRelations;
	}

	public ClusterNode getClustering() {
		return clustering;
	}

	public ArrayList<ClusterNode> getPartition() {
		return partition;
	}

	/*
	 * =============================================================================
	 * === Graphes
	 * =============================================================================
	 */

	// graphe de couplage pondéré
	public String getWeightedCouplingGraph() {
		final StringBuilder graphe = new StringBuilder("di" + Utils.GRAPH_HEADER + "ranksep=3;\n");
		weightedCoupling.forEach((caller, counter) -> {
			counter.forEach((callee, weight) -> {
				graphe.append('"' + caller + "\" -> \"" + callee + "\" [headlabel=\""
						+ Utils.DECIMAL_FORMATTER.format(weight) + "\"];\n");
			});
		});
		graphe.append('}');

		GraphvizGrapher grapher = new GraphvizGrapher("WeightedCoupling_" + projectId, GraphvizGrapher.LayoutEngine.DOT,
				GraphvizGrapher.FileFormat.PDF);
		grapher.setGraphe(graphe.toString());

		return grapher.composerGraphe(true);
	}

	// graphe de regroupement
	public String getClusteringGraph() {
		GraphvizGrapher grapher = new GraphvizGrapher("Clustering_" + projectId, GraphvizGrapher.LayoutEngine.DOT,
				GraphvizGrapher.FileFormat.PDF);
		grapher.setGraphe(clustering.toGraph());

		return grapher.composerGraphe(true);
	}

	// graphe de partitionnement
	public String getPartitionGraph() {
		StringBuilder sb = new StringBuilder(Utils.GRAPH_HEADER);
		for (ClusterNode partition : partition) {
			sb.append("subgraph cluster" + partition.hashCode() + partition.toGraph());
		}
		sb.append('}');

		GraphvizGrapher grapher = new GraphvizGrapher("Partition_" + projectId, GraphvizGrapher.LayoutEngine.DOT,
				GraphvizGrapher.FileFormat.PDF);
		grapher.setGraphe(sb.toString());

		return grapher.composerGraphe(true);
	}
}
