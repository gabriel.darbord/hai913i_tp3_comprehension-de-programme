package processing.spoon.processors;

import java.util.HashMap;
import java.util.HashSet;

import processing.spoon.visitors.CouplingVisitor;
import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtClass;

public class CouplingProcessor extends AbstractProcessor<CtClass<?>> {
	private CouplingVisitor couplingVisitor;

	public CouplingProcessor(HashSet<String> types) {
		couplingVisitor = new CouplingVisitor(types);
	}

	public void process(CtClass<?> element) {
		System.out.println("caller=" + element.getQualifiedName());
		couplingVisitor.setCaller(element.getQualifiedName());
//		couplingVisitor.scan(element);
		element.accept(couplingVisitor);
	}

	public <T> void process(CtInvocation<T> invocation) {
		System.out.println("process invocation");
	}

	/*
	 * retourne les poids des relations entre les classes le poids obtenu est
	 * directionnel : A -> B et B -> A
	 */
	public HashMap<String, HashMap<String, Double>> getWeightedCoupling() {
		HashMap<String, HashMap<String, Double>> res = new HashMap<>();
		HashMap<String, HashMap<String, Integer>> coupling = couplingVisitor.getCoupling();
		final double nbRelations = couplingVisitor.getNbRelations();

		coupling.forEach((caller, value) -> {
			HashMap<String, Double> weights = new HashMap<>();
			res.put(caller, weights);

			value.forEach((callee, count) -> {
				// poids du couplage de cette relation
				double weight = (double) count / nbRelations;

				weights.put(callee, weight);
			});
		});

		return res;
	}

}
