package processing.spoon.processors;

import java.util.HashSet;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtClass;

public class ClassProcessor extends AbstractProcessor<CtClass<?>> {
	private HashSet<String> types = new HashSet<>();

	public HashSet<String> getTypes() {
		return types;
	}

	@Override
	public void process(CtClass<?> ctClass) {
		types.add(ctClass.getQualifiedName());
	}

}