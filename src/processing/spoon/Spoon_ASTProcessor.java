package processing.spoon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import processing.AbstractASTProcessor;
import spoon.Launcher;
import spoon.reflect.CtModel;
import spoon.reflect.code.CtAbstractInvocation;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.filter.AbstractFilter;

public class Spoon_ASTProcessor extends AbstractASTProcessor {

	@Override
	public void analyze(String projectPath) throws IOException {
		super.analyze("Spoon_" + projectPath);

		String projectSourcePath = projectPath + "/src";
		Launcher launcher = new Launcher();

		// classpath du projet
//		launcher.getEnvironment().setNoClasspath(true);
		launcher.getEnvironment().setSourceClasspath(new String[] { projectSourcePath });

		// ajout des fichiers
		launcher.addInputResource(projectSourcePath);

		// construction du modèle
		CtModel model = launcher.buildModel();

		// récupération de l'ensemble des classes du programme
		// et début de construction de la carte de couplage
		HashSet<String> types = new HashSet<>();
		HashMap<String, HashMap<String, Integer>> coupling = new HashMap<>();

		model.getAllTypes().forEach((type) -> {
			String typeName = type.getQualifiedName(); 
			types.add(typeName);
			coupling.put(typeName, new HashMap<>());
		});

		// récupération de toutes les invocations
		/*
		 * Remarque : nous avons essayé d'utiliser CtProcessor et CtVisitor
		 * mais n'avons pas réussi à les faire fonctionner comme attendu
		 * (voir processing.spoon.visitors et processing.spoon.processors).
		 */
		ArrayList<CtAbstractInvocation<?>> invocations = new ArrayList<>();
		invocations.addAll(model.getElements(new AbstractFilter<CtAbstractInvocation<?>>() {
			@Override
			public boolean matches(CtAbstractInvocation<?> invocation) {
				return true;
			}
		}));

		for (CtAbstractInvocation<?> invocation : invocations) {
			String caller = invocation.getParent(CtType.class).getQualifiedName();
			String callee = "?";

			CtTypeReference<?> calleeRef = invocation.getExecutable().getDeclaringType();
			if (calleeRef != null) {
				callee = calleeRef.getQualifiedName();
			}

			if (!types.contains(callee) || caller.contentEquals(callee))
				continue;

			// incrémentation du compteur de relations entre deux classes
			HashMap<String, Integer> counter = coupling.get(caller);
			int count = counter.getOrDefault(callee, 0);
			counter.put(callee, count + 1);
			nbRelations++;
		}

		// calcul du couplage pondéré
		weightedCoupling = new HashMap<>();

		coupling.forEach((caller, value) -> {
			HashMap<String, Double> weights = new HashMap<>();
			weightedCoupling.put(caller, weights);

			value.forEach((callee, count) -> {
				double weight = (double) count / nbRelations;
				weights.put(callee, weight);
			});
		});

		// analyse des couplages
		couplingAnalysis();
	}

}
