package processing.spoon.visitors;

import java.util.HashMap;
import java.util.HashSet;

import spoon.reflect.code.CtConstructorCall;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.code.CtSuperAccess;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.CtScanner;

public class CouplingVisitor extends CtScanner {
	private HashSet<String> types = new HashSet<>();
	private HashMap<String, HashMap<String, Integer>> coupling = new HashMap<>();
	private int nbRelations = 0;
	private String caller;

	public CouplingVisitor(HashSet<String> types) {
		this.types = types;
	}

	/*
	 * =============================================================================
	 * === Accesseurs
	 * =============================================================================
	 */

	public void setCaller(String caller) {
		this.caller = caller;
		coupling.put(caller, new HashMap<>());
	}

	public int getNbRelations() {
		return nbRelations;
	}

	public HashMap<String, HashMap<String, Integer>> getCoupling() {
		return coupling;
	}

	/*
	 * =============================================================================
	 * === Visite
	 * =============================================================================
	 */

	public <T> void visitCtClass(CtClass<T> clazz) {
//		System.out.println("woot " + clazz.getQualifiedName());
	}

	@Override
	public <T> void visitCtInvocation(CtInvocation<T> invocation) {
		System.out.println("invocation");
		visit(invocation.getType());
	}

	@Override
	public <T> void visitCtConstructorCall(CtConstructorCall<T> constructorCall) {
		System.out.println("constructor");
		visit(constructorCall.getType());
	}

	@Override
	public <T> void visitCtSuperAccess(CtSuperAccess<T> superAccess) {
		System.out.println("super");
		visit(superAccess.getType());
	}

	public <T> void visit(CtTypeReference<T> typeReference) {
		String callee = "?";

		if (typeReference != null) {
			callee = typeReference.getQualifiedName();
		}
		System.out.println("callee=" + callee);
		// pas de couplage sur les classes externes au projet
		if (!types.contains(callee))
			return;

		// pas de couplage réflexif
		if (caller.contentEquals(callee))
			return;

		// incrémentation du compteur de relations entre deux classes
		HashMap<String, Integer> counter = coupling.get(caller);
		int count = counter.getOrDefault(callee, 0);
		counter.put(callee, count + 1);

		// incrémentation du compteur total de relations
		nbRelations++;
	}

}
