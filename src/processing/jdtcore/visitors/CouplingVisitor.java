package processing.jdtcore.visitors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class CouplingVisitor extends ASTVisitor {
	private HashSet<String> types = new HashSet<>();
	private HashMap<String, HashMap<String, Integer>> coupling = new HashMap<>();
	private String caller;
	private int nbRelations = 0;

	public CouplingVisitor(List<TypeDeclaration> types) {
		for (TypeDeclaration type : types) {
			this.types.add(type.resolveBinding().getQualifiedName());
		}
	}

	// retourne les poids des relations entre les classes
	// le poids obtenu est directionnel : A -> B et B -> A
	public HashMap<String, HashMap<String, Double>> getWeightedCoupling() {
		HashMap<String, HashMap<String, Double>> res = new HashMap<>();

		coupling.forEach((caller, value) -> {
			HashMap<String, Double> weights = new HashMap<>();

			value.forEach((callee, count) -> {
				// poids du couplage de cette relation
				double weight = (double) count / nbRelations;
				weights.put(callee, weight);
			});

			res.put(caller, weights);
		});

		return res;
	}

	public int getNbRelations() {
		return nbRelations;
	}

	public boolean visit(TypeDeclaration node) {
		caller = node.resolveBinding().getQualifiedName();
		coupling.put(caller, new HashMap<>());
		return true;
	}

	/*
	 * =============================================================================
	 * une invocation de méthode d'une autre classe prend une des formes suivantes :
	 * - MethodInvocation
	 * - SuperMethodInvocation
	 * - ClassInstanceCreation
	 * - SuperConstructorInvocation
	 * =============================================================================
	 */

	// invocation de méthode
	public boolean visit(MethodInvocation node) {
		return visitInvocation(node.resolveMethodBinding());
	}

	// invocation de super méthodes
	public boolean visit(SuperMethodInvocation node) {
		return visitInvocation(node.resolveMethodBinding());
	}

	// instanciation de classe avec 'new'
	public boolean visit(ClassInstanceCreation node) {
		return visitInvocation(node.resolveConstructorBinding());
	}

	// invocation de super constructeur
	public boolean visit(SuperConstructorInvocation node) {
		return visitInvocation(node.resolveConstructorBinding());
	}

	public boolean visitInvocation(IMethodBinding method) {
		String callee = "?";

		if (method != null) {
			ITypeBinding type = method.getDeclaringClass();
			if (type != null) {
				callee = type.getQualifiedName();
			}
		}

		// pas de couplage sur les classes externes au projet
		if (!types.contains(callee))
			return true;

		// pas de couplage réflexif
		if (caller.contentEquals(callee))
			return true;

		// incrémentation du compteur de relations entre deux classes
		HashMap<String, Integer> counter = coupling.get(caller);
		int count = counter.getOrDefault(callee, 0);
		counter.put(callee, count + 1);

		// incrémentation du compteur total de relations
		nbRelations++;

		return true;
	}

}
