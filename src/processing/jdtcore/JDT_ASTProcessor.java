package processing.jdtcore;

import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.jdt.core.dom.CompilationUnit;

import processing.AbstractASTProcessor;
import processing.jdtcore.visitors.CouplingVisitor;
import processing.jdtcore.visitors.TypeDeclarationVisitor;

public class JDT_ASTProcessor extends AbstractASTProcessor {

	// analyse le projet, conserve les infos obtenues et construit le graphe d'appel
	// retourne le nombre de lignes de l'application
	@Override
	public void analyze(String projectPath) throws IOException {
		super.analyze("JDT_" + projectPath);

		// le parser produit les AST des fichiers du projet
		Parser parser = new Parser(projectPath);
		ArrayList<CompilationUnit> asts = parser.getASTs();

		// visite des ASTs
		TypeDeclarationVisitor typeVisitor = new TypeDeclarationVisitor();
		for (CompilationUnit ast : asts) {
			ast.accept(typeVisitor);
		}

		CouplingVisitor couplingVisitor = new CouplingVisitor(typeVisitor.getTypes());
		for (CompilationUnit ast : asts) {
			ast.accept(couplingVisitor);
		}

		weightedCoupling = couplingVisitor.getWeightedCoupling();
		nbRelations = couplingVisitor.getNbRelations();

		// analyse des couplages
		couplingAnalysis();
	}

}
