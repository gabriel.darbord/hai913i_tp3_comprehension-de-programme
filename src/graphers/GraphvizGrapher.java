package graphers;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class GraphvizGrapher {
	private static final String FILE_SEP = File.separator;

	private String nomGraphe; // nom du graphe
	private LayoutEngine moteur; // nom du moteur de mise en page
	private FileFormat format; // format du fichier composé

	// contient le code du graphe
	private String graphe;

	/*
	 * =============================================================================
	 * === Enumerations
	 * =============================================================================
	 */

	// énumération exhaustive des moteurs de mise en page de Graphviz
	public enum LayoutEngine {
		DOT("dot"), NEATO("neato"), TWOPI("twopi"), CIRCO("circo"), FDP("fdp"), OSAGE("osage"), PATCHWORK("patchwork"),
		SFDP("sfdp");

		private final String layoutEngine;

		private LayoutEngine(String layoutEngine) {
			this.layoutEngine = layoutEngine;
		}

		@Override
		public String toString() {
			return layoutEngine;
		}
	}

	// énumération non-exhaustive des formats de sortie disponibles
	public enum FileFormat {
		GIF("gif"), JPEG("jpeg"), JSON("json"), PDF("pdf"), PNG("png"), SVG("svg");

		private final String fileFormat;

		private FileFormat(String fileFormat) {
			this.fileFormat = fileFormat;
		}

		@Override
		public String toString() {
			return fileFormat;
		}
	}

	/*
	 * =============================================================================
	 * === Constructeur
	 * =============================================================================
	 */

	public GraphvizGrapher(String nomGraphe, LayoutEngine moteur, FileFormat format) {
		this.nomGraphe = nomGraphe;
		this.moteur = moteur;
		this.format = format;
	}

	/*
	 * =============================================================================
	 * === Accesseur
	 * =============================================================================
	 */

	public void setGraphe(String graphe) {
		this.graphe = graphe;
	}

	/*
	 * =============================================================================
	 * === Composition
	 * =============================================================================
	 */

	// utilise le moteur de mise en page en paramètre pour la composition
	// crée deux fichiers : - un '.dot' contenant le code du graphe
	// - un fichier au format spécifié contenant la composition, retourne son chemin
	// si redo est faux, vérifie si un fichier a déjà été généré pour le réutiliser
	public String composerGraphe(boolean redo) {
		// si le graphe a déjà été composé
		if (!redo) {
			File fichier = new File("target", nomGraphe + "." + format);
			if (fichier.exists())
				return fichier.getAbsolutePath();
		}

		// dossier qui va contenir les fichiers
		File cible = new File("target");
		cible.mkdir();

		try {
			// récupération ou création du fichier
			File fichier = new File(cible, nomGraphe + ".dot");

			if (redo || !fichier.exists()) {
				fichier.createNewFile();

				// écriture du code du graphe dans le fichier
				FileUtils.writeStringToFile(fichier, graphe.toString(), "UTF-8");
			}

			// composition du graphe déléguée au moteur de mise en page
			String cmd = moteur + " target" + FILE_SEP + nomGraphe + ".dot -T" + format + " -o target" + FILE_SEP
					+ nomGraphe + "." + format;
			if (Runtime.getRuntime().exec(cmd).waitFor() != 0) {
				return "échec : code du graphe mal formé ou format inconnu";
			}
		} catch (IOException e) {
			return "échec : " + e.getMessage();
		} catch (InterruptedException e) {
			return "interrompu : " + e.getMessage();
		} finally {
			// réinitialisation du graphiste
			graphe = null;
		}

		return cible.getAbsolutePath() + FILE_SEP + nomGraphe + "." + format;
	}

}
