package main;

import java.io.IOException;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import processing.jdtcore.JDT_ASTProcessor;
import processing.spoon.Spoon_ASTProcessor;
import ui.CommandLineInterface;

public class StaticAnalysisLauncher {

	public static void main(String[] args) {
		Options options = configParameters();
		CommandLineParser parser = new DefaultParser();
		CommandLine line = null;
		try {
			line = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}

		// récupération du chemin du projet à analyser
		String projectPath = line.getOptionValue("path");
		projectPath = Paths.get(projectPath).toAbsolutePath().normalize().toString();

		try {
			if (line.hasOption("spoon")) {
				new CommandLineInterface(projectPath, new Spoon_ASTProcessor());
			} else {
				new CommandLineInterface(projectPath, new JDT_ASTProcessor());
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private static Options configParameters() {
		Option pathOption = Option.builder("path").hasArg().required().desc("path to the program to analyze").build();
		Option spoonOption = Option.builder("spoon").desc("use Spoon instead of JDT").build();

		Options options = new Options();
		options.addOption(pathOption);
		options.addOption(spoonOption);

		return options;
	}

}
