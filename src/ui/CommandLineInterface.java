package ui;

import java.io.IOException;

import processing.AbstractASTProcessor;
import utils.Utils;

public class CommandLineInterface {
	protected static final String LN = System.lineSeparator();

	protected AbstractASTProcessor processor;

	public CommandLineInterface(String projectPath, AbstractASTProcessor processor) throws IOException {
		this.processor = processor;

		System.out.println("=== Projet : " + projectPath + LN + "= Analyse en cours...");
		processor.analyze(projectPath);

		System.out.println("= Analyse terminée !" + LN);
		afficherMetriques();

		System.out.println(LN + "= Construction des graphes...");
		afficherGraphes();
	}

	protected void afficherMetriques() {
		System.out.println("nombre de relations : " + processor.getNbRelations() + LN//
				+ "couplage pondéré : " + processor.getWeightedCoupling() + LN//
				+ "clustering : " + processor.getClustering() + LN//
				+ "partitionnement : " + processor.getPartition());
	}

	protected void afficherGraphes() {
		// création des graphes
		String weightedCouplingGraph = processor.getWeightedCouplingGraph();
		String clusteringGrah = processor.getClusteringGraph();
		String partitionGraph = processor.getPartitionGraph();

		// affichage des liens vers les graphes
		System.out.println("= Graphes terminés !" + LN + LN//
				+ "couplage pondéré : " + weightedCouplingGraph + LN//
				+ "clustering : " + clusteringGrah + LN//
				+ "partitionnement : " + partitionGraph);

		// ouverture des fichiers
		Utils.desktopOpen(weightedCouplingGraph);
		Utils.desktopOpen(clusteringGrah);
		Utils.desktopOpen(partitionGraph);
	}
}
